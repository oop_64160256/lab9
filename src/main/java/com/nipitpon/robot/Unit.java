package com.nipitpon.robot;

public class Unit {
    private int x;
    private int y;
    private boolean dominate;
    private char symbol;
    private Map map;

    public Unit(Map map, char symbol, int x, int y, boolean dominate) {
        this.x = x;
        this.y = y;
        this.dominate = dominate;
        this.symbol = symbol;
        this.map = map;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }

    public boolean setX(int x) {
        if (!map.isInWidth(x)) return false;
        if (map.hasDominate(x, y)) return false;
        this.x = x;
        return true;
    }

    public boolean setY(int y) {
        if (!map.isInHeight(y)) return false;
        if (map.hasDominate(x, y)) return false;
        this.y = y;
        return true;
    }

    public boolean setXY(int x, int y) {
        if (!map.isOn(x, y)) return false;
        if (map.hasDominate(x, y)) return false;
        this.x = x;
        this.y = y;
        return true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean isDominate() {
        return dominate;
    }

    public Map getMap() {
        return map;
    }

    public String toString() {
        return "Unit(" + this.symbol + ") [" + this.x + ", " + this.y + "] is on " + map;
    }

    public char getSymbol() {
        return symbol;
    }

}
